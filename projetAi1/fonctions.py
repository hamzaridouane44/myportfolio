from skimage.feature import graycomatrix, graycoprops
from mahotas import features
from BiT import bio_taxo
import cv2
import numpy as np


def glcm(file):  # file -> Gray-scale image
    features = []
    co_matrix = graycomatrix((file), [1], [0], levels=None, symmetric=True, normed=True)
    diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
    features.append(diss)
    cont = graycoprops(co_matrix, 'contrast')[0, 0]
    features.append(cont)
    ener = graycoprops(co_matrix, 'energy')[0, 0]
    features.append(ener)
    homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
    features.append(homo)
    corr = graycoprops(co_matrix, 'correlation')[0, 0]
    features.append(corr)
    asm = graycoprops(co_matrix, 'ASM')[0, 0]
    features.append(asm)
    return features


# return np.asarray(features)

def glcm2(file):  # file -> Gray-scale image
    features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angles in angles:
        co_matrix = graycomatrix((file), [1], [angles], levels=None, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        features.append(asm)
    return features.toarray()


# return np.asarray(features)

def haralick(file):
    haralick_feat = features.haralick(file).mean(0)
    return haralick_feat.tolist()


def BiT_desc(file):
    bit_features = bio_taxo(file)
    return bit_features


def all_descriptors(file):
    glcm_feat = glcm(file)
    haralick_feat = haralick(file)
    bit_features = BiT_desc(file)
    return glcm_feat + haralick_feat + bit_features



