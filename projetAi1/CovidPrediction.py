#---------------------------------------------Importation--------------------------------------------------------
import os
import cv2
import numpy as np
from os import listdir
from typing import List
from fonctions import glcm , haralick, BiT_desc
import streamlit as st
import pandas as pd
from PIL import Image
from pandas import read_csv
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import Binarizer
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold, cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier as CART
from sklearn.svm import SVC
from mahotas import features
from BiT import bio_taxo

from sklearn.linear_model import ElasticNet
#-----------------------------------------------Fin---------------------------------------------------------

st.sidebar.header('Parametres d entrée  des utilisateurs' )
algo = st.sidebar.selectbox('le type de problème', ('Classification', ))

#-------------------------------------Lire l'image-------------------------------------------------
def load_image(image_file):
    img = Image.open(image_file)
    return img
file = st.file_uploader("Please choose a file", type=["png","jpg","jpeg"])
if file is not None:
    filed = {"Nom du fichier": file.name, "filetype": file.type, "filesize":file.size}
    st.write(filed)
    st.image(load_image(file),width=300)
    # Saving upload
    with open(os.path.join("ressources", file.name), "wb") as f:
        f.write((file).getbuffer())
    st.success("File Saved succefully")
#----------------------------------Fin----------------------------------------------------------------

#-------------------------------------Gerer les parametre-------------------------------------------------
if algo == 'Classification':
    ImageTrans = st.sidebar.selectbox(
        'Choisir le type de description de caracteristique :',
        ('Bit_desc', "GLCM", "haralick"))
    Models = st.sidebar.selectbox('Choisir le type d algorithme', ("LogisticRegression", "LinearDiscriminantAnalysis", "KNeighborsClassifier", "GaussianNB", "DecisionTreeClassifier"))
    Transform = st.sidebar.selectbox('Choisir le type de la transformation', ("Rescale", "Standardized", "Normalization", "Binarization"))
    aff = st.sidebar.selectbox('Choisir la métrique', ("accuracy", "recall", "precision", "f1"))
    if Models == "LogisticRegression":
        Mod = LogisticRegression()
    elif Models == "LinearDiscriminantAnalysis":
        Mod = LDA()
    elif Models == "KNeighborsClassifier":
        Mod = KNN()
    elif Models == "GaussianNB":
        Mod = GaussianNB()
    elif Models == "DecisionTreeClassifier":
        Mod = CART()

if Transform == "Rescale":
    trans = MinMaxScaler()
elif Transform == "Standardized":
    trans = StandardScaler()
elif Transform == "Normalization":
    trans = Normalizer()
elif Transform == "Binarization":
    trans = Binarizer()
#--------------------------------------------Fin-------------------------------------------------------------

#------------------------------------Fonction de la classification---------------------------------------
def classification(transformation, models):
    def metrics(conf_matrix):
        TN = matrix[0, 0]
        FP = matrix[0, 1]
        FN = matrix[1, 0]
        TP = matrix[1, 1]
        accuracy = (TP + TN) / (TP + TN + FN + FP)
        recall = TP / (TP + FN)
        precision = TP / (TP + FP)
        f1 = 2 * ((recall * precision) / (precision + recall))
        return accuracy, recall, precision, f1
    model = models
    if ImageTrans == "GLCM":
        filename = "Data_GLCM.csv"
    if ImageTrans == "haralick":
        filename = "Data_Haralick.csv"
    if ImageTrans == "Bit_desc":
        filename = "Data_Bit.csv"
    if ImageTrans == "GLCM":
        glccm(file)
    if ImageTrans == "haralick":
        harralick(file)
    if ImageTrans == "Bit_desc":
        BiT_descc(file)
    dataframe = read_csv(filename)
    value = dataframe.values
    X = value[ : , 0:-1]
    Y = value[ : , -1]
    seed = 7
    scaler = transformation
    transformedX = scaler.fit_transform(X)
    X_train,X_test,Y_train,Y_test = train_test_split(transformedX,Y, random_state=seed)
    model.fit(X_train,Y_train)
    filename = 'data/imageData.csv'
    dataframe = read_csv(filename)
    value = dataframe.values
    df = value[ : , :-1]
    predicted = model.predict(X_test)
    matrix = confusion_matrix(Y_test, predicted)
    affValue=0
    affName=""
    acc, rec, prec, f1 = metrics(matrix)
    if aff =="accuracy":
        affName="Accuracy"
        affValue=(acc*100).round(2)
    elif aff == "recall":
        affValue=(rec*100).round(2)
        affName = "Recall"
    elif aff == "precision":
        affValue=(prec*100).round(2)
        affName = "Precision"
    elif aff == "f1":
        affValue=(f1*100).round(2)
        affName = "F1-Score"
    prediction = model.predict(df)
    prediction_proba = model.predict_proba(df)
    st.subheader('Prediction probability :')
    st.write(prediction_proba)
    st.write('-------------------------------')
    if prediction == 0:
        pedictionName = "Non Covid STAAY"
    else:
        pedictionName = "COVID RUUUUUUUUUUUUUN"
    st.subheader('La prediction :')
    st.write(pedictionName)
    st.write('-------------------------------')
    st.subheader("Metrique choisie : " +affName )
    st.write(affValue)
    st.write('-------------------------------')
    st.subheader("Now, you are using :")
    st.write("Algorythme: " + algo)
    st.write("Type of transformation: " + Transform)
    st.write("Metric: " + aff)
    st.write("Value of the description (Image): ")
    st.write(df)
#----------------------------------------Fin-------------------------------------------------------------

#------------------------------------glcm pour le file----------------------------------------------------
def glccm(file):
    ffile = "ressources/" + file.name
    img = cv2.imread(ffile, 0)
    feat = glcm(img)
    features = []
    features.append(feat)
    dataframe = pd.DataFrame.from_records(data=features)
    dataframe.to_csv('data/imageData.csv', sep=',')
    st.write('CSV glcm Generated succefully !')
#-------------------------------------------Fin----------------------------------------------------------

#------------------------------------haralick pour le file----------------------------------------------------
def harralick(file):
    ffile = "ressources/" + file.name
    img = cv2.imread(ffile, 0)
    haralick_feat = haralick(img)
    ft = []
    ft.append(haralick_feat)
    dataframe = pd.DataFrame.from_records(data=ft)
    dataframe.to_csv('data/imageData.csv', sep=',')
    st.write('CSV har Generated succefully !')

#----------------------------------------Fin-------------------------------------------------------------

#------------------------------------bit_desc pour le file----------------------------------------------------
def BiT_descc(file):
    ffile = "ressources/" + file.name
    img = cv2.imread(ffile, 0)
    bit_features = bio_taxo(img)
    features = []
    features.append(bit_features)
    dataframe = pd.DataFrame.from_records(data=features)
    dataframe.to_csv('data/imageData.csv', sep=',')
    st.write('CSV bit Generated succefully !')

#----------------------------------------Fin-------------------------------------------------------------

#------------------------------------------Executer-------------------------------------------------------
if (file!= None  ):

    classification(trans, Mod)

#-------------------------------------------Fin-------------------------------------------------------------


#---------------------------------------Transformer les images selon le choix-------------------------------------
# def main():
#     files_path = "Covid/"
#     files_dir: List[str] = listdir(files_path)
#     features = list()
#     count = 1
#     print(files_dir)
#     for testCovid in files_dir:
#         # print(testCovid, '\n---------------\n')
#         for imageName in listdir(files_path + testCovid + "/"):
#             print('# of extracted images :%s' % len(features))
#             # print (files_path + testCovid +"/"+imageName)
#             img = cv2.imread(files_path + testCovid + "/" + imageName, 0)
#             if ImageTrans== "GLCM":
#                 feat = glcm(img)
#             if ImageTrans=="haralick":
#                 feat= haralick(img)
#             if ImageTrans=="Bit_desc":
#                 feat=BiT_desc(img)
#             if testCovid == "CT_COVID":
#                 classe = 1
#                 feat += [1]
#             else:
#                 classe = 0
#                 feat += [0]
#             features.append(feat)
#             count += 1
#     print('Total # images : ', len(features))
#     # convert features list to dataframe
#     dataframe = pd.DataFrame.from_records(data=features)
#     print('Shape : ', dataframe.shape)
#     # convert dataframe to csv
#     dataframe.to_csv('Covid_image.csv', sep=',')
#     print('CSV CREATED')
#
#
# if __name__ == "__main__":
#     main()
#---------------------------------------------Fin-----------------------------------------------------------------












