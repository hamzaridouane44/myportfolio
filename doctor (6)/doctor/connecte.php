<?php 

session_start();

	$user = 'root';   // le nom de l'utilisateur
    $pass = '';       // le mot de passe 
    $host = 'localhost';  // le serveur de base de données
    $bdd = 'bdd_hopital';  // le nom de la base de données 


    $link = mysqli_connect($host, $user, $pass, $bdd) or die("Erreur de connexion au serveur");


    mysqli_select_db($link, $bdd) or die("Erreur de connexion a la BDD");


	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		//something was posted
		$user_name = $_POST['login'];
		$password = $_POST['password'];

		if(!empty($user_name) && !empty($password) && !is_numeric($user_name))
		{

			//read from database
			$query = "select * from patient where Username = '$user_name' limit 1";
			$result = mysqli_query($link, $query);

			if($result)
			{
				if($result && mysqli_num_rows($result) > 0)
				{

					$user_data = mysqli_fetch_assoc($result);
					
					if($user_data['Password'] === $password)
					{

						$_SESSION['CodeUtilisateur'] = $user_data['CodeUtilisateur'];
				     	header("Location:indexUtilisateur.php");
             
            
					}
				}
			}
			
		      echo"<script type='text/javascript'>alert('Votre login et/ou password sont incorrectes !');</script>";
		}else
		{
      echo"<script type='text/javascript'>alert('Votre login et/ou password sont incorrectes !');</script>";
		}
	}
  
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>DOCTOR &amp; 98 </title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,800,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=BenchNine:300,400,700' rel='stylesheet' type='text/css'>
	<script src="js/modernizr.js"></script>
	<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
        body{
            background:url("img/hopital3.jpg");
            background-size:cover;
            background-repeat:no-repeat;
            font-family:Arial, Helvetica,sans-serif;
        }
        .auto-style1 {
            background: #fff;
            position: fixed;
            width: 106%;
            z-index: 999;
            left: -48px;
            top: 0px;
            height: 102px;
        }
    </style>

</head>
</head>
<body>
    <header class="auto-style1">
		<div class="container">
			<div class="row">
				<div class="col-xs-5 header-logo">
					<a href="index.html"><img src="img/logo.png" alt="" class="img-responsive logo"></a><br>
					&nbsp;</div>

				<div class="col-md-7">
					<nav class="navbar navbar-default">
					  <div class="container-fluid nav-bar">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">98</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      
					      <ul class="nav navbar-nav navbar-right">
					        <li><a class="menu active" href="#home" >Home</a></li>
					      </ul>
					    </div><!-- /navbar-collapse -->
					  </div><!-- / .container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</header>
    <br>
    <br>
    <br>
    <br>
    <br>
<center>
        <form method="POST">

                <div class="cont">
                <h2 class="h" >Connexion</h2>

				<input class="cas" type="text"  placeholder="Entrer votre login" name="login" ><br>
             
                <input class="cas" type="password"  placeholder="Entrer le mot de passe" name="password" ><br>
      

				<a href="#"><button class="button" name="connexion">Se connecter</button>
				<br>
				<a href="Inscription.php"><input type="button" value="S'inscrire" class="button" name="Inscription"/></a>
				<br>
				<br>

        </div>
    </center>

	<footer class="footer clearfix">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 footer-para">
					<p>&copy;Mostafizur All right reserved</p>
				</div>
				<div class="col-xs-6 text-right">
					<a href=""><i class="fa fa-facebook"></i></a>
					<a href=""><i class="fa fa-twitter"></i></a>
					<a href=""><i class="fa fa-skype"></i></a>
				</div>
			</div>
		</div>
	</footer>

    <script src="js/jquery-2.1.1.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/custom.js"></script>        
    
</body>
</html>