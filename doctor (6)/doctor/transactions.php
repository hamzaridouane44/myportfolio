<!DOCTYPE html>
<html lang="en"> 
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>DOCTOR - Responsive HTML &amp; Bootstrap Template</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,800,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=BenchNine:300,400,700' rel='stylesheet' type='text/css'>
	<script src="js/modernizr.js"></script>
	<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
        .auto-style1 {
            background: #fff;
            position: fixed;
            width: 100%;
            z-index: 999;
            left: -16px;
            top: 0px;
            height: 102px;
        }
        .auto-style2 {
            width: 20px;
            height: 105px;
            margin: auto;
           /* background: ;*/
        }
       
    </style>

</head>
<body>
	
	<!-- ====================================================
	header section -->
	<header class="auto-style1">
		<div class="container">
			<div class="row">
				<div class="col-xs-5 header-logo">
					<a href="index.html"><img src="img/logo.png" alt="" class="img-responsive logo"></a><br>
					&nbsp;</div>

				<div class="col-md-7">
					<nav class="navbar navbar-default">
					  <div class="container-fluid nav-bar">
					        <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					    </div>

					  <!-- Collect the nav links, forms, and other content for toggling -->
					  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      
					      <ul class="nav navbar-nav navbar-right">
					         <li><a class="menu active" href="indexAdmin.php" >Home</a></li>
							 
							 <li class="nav-item dropdown">
                                <a   class="menu active nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Surveiller</a>
                               <div class="dropdown-menu">
								   <center>
									<h3><a class="menu active" href="transactions.php">Rapport des tans.</a><hr>
										<li><a class="menu active" href="diagnostique.php">Rapport des nominations</a></li> </h3><ul>
									  
									  
								   </center>
                                
                                </div>
								     
								  
                            </li>

							<li><a class="menu active" href="#" >Log Out</a></li>
                            <li class="nav-item dropdown">
                                <a   class="menu active nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Gérer</a>
                               <div class="dropdown-menu">
							   <center>
									<h3><a class="menu active" href="depHopital.php">Dep. Hôpital</a><hr>

									   <li><a class="menu active" href="utilisateur.php">Comptes</a></li><hr>
									   <li><a class="menu active" href="medcin.php">Medcins</a></li><hr>
									   <li><a class="menu active" href="infermier.php">Infermiers</a></li><hr>
								
								   </center>
								   
                                
                                </div>
								     
								
                            </li>
							
							
					      </ul>
					    </div><!-- /navbar-collapse -->
					  </div><!-- / .container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</header> <!-- end of header area -->

	<section class="slider" id="home">
		<div class="container-fluid">
			<div class="row">
			    <div id="carouselHacked" class="carousel slide carousel-fade" data-ride="carousel">
					<div class="auto-style2" ></div>
                    <center>
                    <form id="form" method="POST" action=""> 
     <h2>Rechercher une transaction  : <input type="text" name='recherche'>
     <input type="SUBMIT" value="Search!"> <br> <br></h2>
     <!--<a href="AjouterRecette.php"><input type="button" style="background-color: black; color:honeydew;" value="Ajouter une recettes! "></a><br> <br>-->
     </form>
      <?php   
     $user = 'root';   // le nom de l'utilisateur
     $pass = '';       // le mot de passe 
     $host = 'localhost';  // le serveur de base de données
     $bdd = 'bdd_hopital';  // le nom de la base de données 
 

$con=mysqli_connect($host,$user,$pass,$bdd);
 //Check connection
if (mysqli_connect_errno())
{
echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$recherche = isset($_POST['recherche']) ? $_POST['recherche'] : '';
echo "<a href='AjouterTransaction.php' style='font-size: 25px;'  border='1' id='tableau'><button>Ajouter Une Transaction</button> </a> <br /><br />";

echo "<table  style='font-size: 25px;'  border='1' id='tableau'>
<tr >
<th>Date Transaction</th>
<th>Montant Transaction</th>
<th>Client</th>
<th>Numero de Transaction</th>
<th>Modifier une Transaction</th>


</tr>";

 // la requete mysql
 $q = $con->query(
    "SELECT * FROM Transaction
     WHERE Client LIKE '%$recherche%'
    ");

while($row = mysqli_fetch_array($q))
{?>
<center>
    
<tr>
    <td width=200px align='center'><?php  echo $row['DateTransaction']; ?></td>
    <td width=200px align='center'><?php echo $row['MontantTransaction']; ?></td>
    <td width=200px align='center'><?php echo $row['Client']; ?></td>
	<td width=200px align='center' ><?php echo $row['NumTransaction']; ?></td>
	<?php  echo "<td width=200px align='center' ><a href='ModifierTransaction.php?rn=$row[NumTransaction]' style='background-color: red; color:honeydew; width:200px;'> Modifier </a></td>"?>
     


</tr>

</center>
<?php
}
echo "</table>";

mysqli_close($con);
?>   </center>

	<!-- footer starts here -->
	<footer class="footer clearfix">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 footer-para">
					<p>&copy;Mostafizur All right reserved</p>
				</div>
				<div class="col-xs-6 text-right">
					<a href=""><i class="fa fa-facebook"></i></a>
					<a href=""><i class="fa fa-twitter"></i></a>
					<a href=""><i class="fa fa-skype"></i></a>
				</div>
			</div>
		</div>
	</footer>

	<!-- script tags
	============================================================= -->
	<script src="js/jquery-2.1.1.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>