<!DOCTYPE html>
<html lang="en"> 
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>DOCTOR &amp;</title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,800,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=BenchNine:300,400,700' rel='stylesheet' type='text/css'>
	<script src="js/modernizr.js"></script>
	<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
<body>
	
	<!-- ====================================================
	header section -->
	<header class="top-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-5 header-logo">
					<br>
					<a href="indexUtilisateur.html"><img src="img/logo.png" alt="" class="img-responsive logo"></a>
				</div>

				<div class="col-md-7">
					<nav class="navbar navbar-default">
					  <div class="container-fluid nav-bar">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      
					      <ul class="nav navbar-nav navbar-right">
					         <li><a class="menu active" href="indexUtilisateur.php" >Home</a></li>
							 <li><a class="menu active" href="Profil.php" >Profil</a></li>
							 <li><a class="menu active" href="#home" >Ordonance</a></li>
                            <li class="nav-item dropdown">
                                <a   class="menu active nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Rendez-Vous</a>
                               <div class="dropdown-menu">
								   <center>
									<h3><a class="menu active" href="listeRV.php">Liste des R.V</a><hr>
										<li><a class="menu active" href="prendreRv.php">Prendre R.V</a><hr></li> 
									   <li><a class="menu active" href="#">Supprimer R.V</a></li></ul>
								   </center>
								   
                                
                                </div>
								     
								  
                            </li>
					      </ul>
					    </div><!-- /navbar-collapse -->
					  </div><!-- / .container-fluid -->
					</nav>
				</div>
                <?php   
     $user = 'root';   // le nom de l'utilisateur
     $pass = '';       // le mot de passe 
     $host = 'localhost';  // le serveur de base de données
     $bdd = 'bdd_hopital';  // le nom de la base de données 
 

$con=mysqli_connect($host,$user,$pass,$bdd);
 //Check connection
if (mysqli_connect_errno())
{
echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$recherche = isset($_POST['recherche']) ? $_POST['recherche'] : '';

echo "<table border='1' id='tableau'>
<tr>
<th>Nom</th>
<th>Date</th>
<th>Le docteur</th>
<th>Hopital</th>
<th>Raison</th>
<th>Action modifier</th>
<th>Action supprimer</th>
</tr>";

 // la requete mysql
 $q = $con->query(
    "SELECT nomComplet,datedebut,docteur,hopital,raison FROM patient
     WHERE nomComplet LIKE '%Hamza Ridouane%'
    ");

while($row = mysqli_fetch_array($q))
{?>
<center>
    
<tr>
    <td><?php  echo $row['nomComplet']; ?></td>
    <td><?php echo $row['datedebut']; ?></td>
    <td><?php echo $row['docteur']; ?></td>
    <td><?php echo $row['hopital']; ?></td>
	<td><?php echo $row['raison']; ?></td>
   <?php  echo "<td><a href='#?rn=$row[NRV]' style='background-color: green; color:honeydew; width:200px;'> Modifier </a></td>"?>
    <?php  echo "<td><a href='#?rn=$row[NRVo]' style='background-color: red; color:honeydew; width:200px;'> Delete </a></td>"?> 
     


</tr>

</center>
<?php
}
echo "</table>";

mysqli_close($con);
?>
			</div>
		</div>
	</header> <!-- end of header area -->
    

    <footer class="footer clearfix">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 footer-para">
					<p>&copy;Mostafizur All right reserved</p>
				</div>
				<div class="col-xs-6 text-right">
					<a href=""><i class="fa fa-facebook"></i></a>
					<a href=""><i class="fa fa-twitter"></i></a>
					<a href=""><i class="fa fa-skype"></i></a>
				</div>
			</div>
		</div>
	</footer>

	<!-- script tags
	============================================================= -->
	<script src="js/jquery-2.1.1.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>