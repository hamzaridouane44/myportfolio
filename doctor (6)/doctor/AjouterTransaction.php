
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>DOCTOR &amp; 98 </title>
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,800,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=BenchNine:300,400,700' rel='stylesheet' type='text/css'>
	<script src="js/modernizr.js"></script>
	<!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <style>
        body{
            background:url("img/hopital3.jpg");
            background-size:cover;
            background-repeat:no-repeat;
            font-family:Arial, Helvetica,sans-serif;
        }
        .auto-style1 {
            background: #fff;
            position: fixed;
            width: 105%;
            z-index: 999;
            left: -36px;
            top: -18px;
            height: 108px;
        }
		.select{
	outline:none;
    background-color:transparent;
    color: white;
    padding:10px 6px;
    font-size:18px;
    font-weight:bold;
		}
		.option{
			color: black;
		}
    </style>

</head>
</head>
<body>
    
<header class="top-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-5 header-logo">
					<br>
					<a href="indexUtilisateur.html"><img src="img/logo.png" alt="" class="img-responsive logo"></a>
				</div>

				<div class="col-md-7">
					<nav class="navbar navbar-default">
					  <div class="container-fluid nav-bar">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      
					      <ul class="nav navbar-nav navbar-right">
					         <li><a class="menu active" href="indexUtilisateur.php" >Home</a></li>
							 <li><a class="menu active" href="Profil.php" >Profil</a></li>
							 <li><a class="menu active" href="#home" >Ordonance</a></li>
                            <li class="nav-item dropdown">
                                <a   class="menu active nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">Rendez-Vous</a>
                               <div class="dropdown-menu">
								   <center>
								   <li><a class="menu active" href="prendreRv.php">Ajouter un R.V</a></li><hr>
									<li><a class="menu active" href="afficherRv.php">Liste des R.V</a><li><hr>
										<li><a class="menu active" href="modifierRv.php">Modifier un  R.V</a><hr></li> 
									   <li><a class="menu active" href="supprimerRv.php">Supprimer R.V</a></li></ul>
									   
								   </center>
								   
                                
                                </div>
								     
								  
                            </li>
					      </ul>
					    </div><!-- /navbar-collapse -->
					  </div><!-- / .container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</header> <!-- end of header area -->
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
	 
	
    
<center>
        <form action="AjouterTransacionphp.php" method="POST">
			
                <div class="cont">
					
                <h2 class="h" >Ajouter une transaction</h2>

                <input class="cas" type="Date" placeholder="Entrer la date de transaction " name="DateTransaction" required="required"><br>
              
                <input class="cas" type="Text" placeholder="Entrer la montant de la transaction " name="MontantTransaction" required="required"><br>

                <input class="cas" type="Text" placeholder="Entrer le nom du client " name="Client" required="required"><br>

                <input class="cas" type="Text" placeholder="Entrer le numero du transaction " name="NumTransaction" required="required"><br>
			
                <button class="button" name="bouton"> Inscrire</button>
        </div>
    </center>

	<footer class="footer clearfix">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 footer-para">
					<p>&copy;Mostafizur All right reserved</p>
				</div>
				<div class="col-xs-6 text-right">
					<a href=""><i class="fa fa-facebook"></i></a>
					<a href=""><i class="fa fa-twitter"></i></a>
					<a href=""><i class="fa fa-skype"></i></a>
				</div>
			</div>
		</div>
	</footer>
    <script src="js/jquery-2.1.1.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/custom.js"></script>        
    
</body>
</html>