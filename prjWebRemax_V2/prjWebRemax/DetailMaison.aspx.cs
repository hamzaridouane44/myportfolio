﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

namespace prjWebRemax
{
    public partial class DetailMaison : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            remaxEntities remax = new remaxEntities();
            if (!IsPostBack)
            {
                int refMaison = Convert.ToInt32(Request.QueryString["refm"]);
                string affichermaisons = "";
                var maisonsimage = from ImgMaison AL in remax.ImgMaison
                                   where AL.RefMaison.ToString() == refMaison.ToString()
                                   select AL;
                ImgMaison i = maisonsimage.First();
                affichermaisons = " <div class='item active' align='center'> <a href='#'  id='Blogue'>";
                affichermaisons += "<img src='" + i.Image_String + "' alt='Blogue'></a> </div>";
                ltlMaison2.Text = affichermaisons;

            
                for (int index = 1; index < maisonsimage.Count(); index++)
                {
                    affichermaisons = " <div class='item ' align='center'> <a href='#'  id='Blogue'>";
                    affichermaisons += "<img style='height:600px; width=400px' src='" + maisonsimage.ToList().ElementAt(index).Image_String+ "' alt='Blogue'></a> </div>";
                    ltlMaison.Text += affichermaisons;
                }

                var maisons = from Maison M in remax.Maison
                              where M.RefMaison.ToString() == refMaison.ToString()
                              select M;
                Maison r = maisons.First();
                lblInfo.Text = r.Type.ToString() + " Région de  " + r.Emplacement.ToString();
                lblinfo2.Text = "Prix : " + r.Prix.ToString() + "$         Nombre de Piece " + r.nbrPiece.ToString();

                var refagents = from Maison_Agents M in remax.Maison_Agents
                                where M.RefMaison.ToString() == refMaison.ToString()
                                select M;

                foreach (Maison_Agents s in refagents)
                {
                    var agents = from Agents M in remax.Agents
                                 where M.RefAgent.ToString()== s.RefAgent.ToString()
                                 select M;
                    foreach (Agents q in agents)
                    {
                        affichermaisons = "     <div class='agent -entry' itemscope itemtype = 'https://schema.org/Organization' >";

                        affichermaisons += "  <meta itemprop='name' content='RE/MAX ALLIANCE'>";

                        affichermaisons += "<a href = 'AgentDetails.aspx?refa=" + q.RefAgent + "' class='agent-thumbnail'>";


                        affichermaisons += "	<img class='media -object img-responsive' height=400px width=250px src='" + q.image+ "' alt='GOKHAN ABAY, RE/MAX ALLIANCE'>";


                        affichermaisons += "	</a>";

                        affichermaisons += " <div class='agent-details' itemprop='member' itemscope itemtype = 'https://schema.org/Person' >";

                        affichermaisons += "  <h2 ><a href=''><span itemprop = 'givenName' >" + q.Nom + "</span></a></h2>";
                        affichermaisons += "  <div class='agent-details-business' itemprop='address' itemscope itemtype = 'https://schema.org/PostalAddress' >";

                        affichermaisons += "   <a href=''>RE/MAX ROYAL  inc. </a>";
                        affichermaisons += "<span itemprop = 'addressLocality' > " + q.Ville + " </ span >";

                        affichermaisons += "  </ div >";

                        affichermaisons += "  <div class='agent-details-contact'>";
                        affichermaisons += "  <address>";
                        affichermaisons += "<abbr title = 'Téléphone' > Bur.:</abbr> <span itemprop = 'telephone' class='phone'>" + q.NumeroTel + " </span>";
                        affichermaisons += "  <br>";

                        affichermaisons += "    <abbr title = 'Cellulaire' > Cell.:</abbr> <span itemprop = 'telephone' class='phone'>" +q.NumeroFixe + " </span>";
                        affichermaisons += "   </address>";
                        affichermaisons += " <div class='btn-group'>";
                        affichermaisons += "  <a href = 'Message.aspx?refa=" + q.RefAgent + "' class='btn btn-transparent-alt agent-contact' data-agent-id='11475'>";

                        affichermaisons += "      Contactez -moi";
                        affichermaisons += "    </a>";
                        affichermaisons += "   <a href = 'AgentDetails.aspx?refa=" + q.RefAgent + "' class='btn btn-transparent-alt'>Voir ma page</a></div></div></div></div><br />";


                        ltlAgent.Text += affichermaisons;
                    }

                }
            }



        }
    }
}