﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

namespace prjWebRemax
{
    public partial class AgentDetails : System.Web.UI.Page
    {
        remaxEntities remax = new remaxEntities();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string affciherAgents = "";
                string affichermaisons = "";
                int refAgent = Convert.ToInt32(Request.QueryString["refa"]);
                

                var agents = from Agents M in remax.Agents
                             where M.RefAgent.ToString() == refAgent.ToString()
                             select M;
                foreach (Agents b in agents)
                {
                    var Langue = from Langue_Agent M in remax.Langue_Agent
                                 where  M.refAgents.ToString() == refAgent.ToString()
                                 select M;

                    affciherAgents += "<div class='agent - wrapper' itemscope itemtype='https://schema.org/LocalBusiness'> <div class='agent-about' itemprop='member' itemscope itemtype = 'https://schema.org/Organization' ><div class='container'><div class='agent - about - inner'> <div class='agent media'> ";

                    affciherAgents += "	<div class='media -left'><table width='1000px' align='center'> <tr><td> ";

                    affciherAgents += "	<img class='media -object' height='345px' width='300px' itemprop='image' src='" + b.image+ "' alt='' /> </td><td>";

                    affciherAgents += "	</div> ";
                    affciherAgents += " <div class='media -body'> ";

                    affciherAgents += "	<h1 itemprop = 'name' >" + b.Nom + "<span class='agent-type'><h4> Courtier immobilier</h4></span></h1> ";

                    affciherAgents += "	<address itemprop = 'address' itemscope itemtype = 'https://schema.org/PostalAddress' > ";

                    affciherAgents += "      <h2 style= 'font -size:18px; font-weight:400;' > ";


                    affciherAgents += "          <h3> <a href= '' > RE / MAX ROYAL (JORDAN) INC. </a></h3><br /> ";
                    affciherAgents += "		<span itemprop = 'streetAddress' >" + b.Numero+ "</span> bureau 203<br />";

                    affciherAgents += "		<span itemprop = 'addressLocality' > Pointe - Claire </ span >, <span itemprop = 'addressRegion' > " + b.Ville+ " </ span >, <span itemprop = 'postalCode' class='postal'>H9R 5M7</span></br>";

                    affciherAgents += "	</h2>";

                    affciherAgents += "      Bur.:";

                    affciherAgents += "    <a href = '' ><span itemprop='telephone' class='phone'>" + b.NumeroTel + " <span class='visible-xs-inline'><i class='fa fa-mobile'></i></span></span></a>";



                    affciherAgents += " 	<br/>";

                    affciherAgents += "      T&eacute;l&eacute;c.: <a href = '' ><span itemprop='faxNumber' class='phone'>" + b.NumeroFixe + " <span class='visible-xs-inline'><i class='fa fa-mobile'></i></span></span></a>";



                    affciherAgents += " 	<br/>";

                    affciherAgents += "      Cell.: <a href = '' ><span itemprop='telephone' class='phone>'" + b.NumeroTel + "<span class='visible-xs-inline'><i class='fa fa-mobile'></i></span></span></a>";

                    affciherAgents += "     <br/> Langue :  <br/> <ul>";

                    foreach (Langue_Agent s in Langue)
                    {
                        affciherAgents += "<li>" + s.Langue + "</li>";
                    }

                    affciherAgents += "   </ul> </address>";
                    affciherAgents += " 	<div class='agent -presentation' itemprop='description'><p>&laquo; Pour un RÉsultat MAXimum, faites confiance à RE/MAX! &raquo;</p></div>";

                    affciherAgents += " 	</div>";
                    affciherAgents += "    </div>";
                    affciherAgents += "    <div class='agent -separation'></div>";

                    affciherAgents += " 	<div class='agent -contact'>";

                    affciherAgents += " 	<a href = 'Message.aspx?refa=" + b.RefAgent + "'class='btn btn-primary btn-lg btn-form-contact' data-agent-id='11475'>Contactez-moi</a>";


                    affciherAgents += " 	</div> ";
                    affciherAgents += "     </div></div></div></div></div></td></tr></table>";
                    ltlAgent.Text = affciherAgents;

                }
                
            

                }
            }
           
       
    }
}