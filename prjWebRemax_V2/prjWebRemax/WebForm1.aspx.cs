﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Data.OleDb;

namespace prjWebRemax
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        remaxEntities remax = new remaxEntities();
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
               
                var critere1 = from Critere c in remax.Critere
                               where c.TypeDeCritere == "1" 
                               select c;
                ddlCategorie.DataSource = critere1.ToList();
                ddlCategorie.DataTextField = "Critere1";
                ddlCategorie.DataBind();

                var critere2 = from Critere c in remax.Critere
                               where c.TypeDeCritere == "2"
                               select c;
                ddlRegion.DataSource = critere2.ToList();
                ddlRegion.DataTextField = "Critere1";
                ddlRegion.DataBind();

                var critere3 = from Critere c in remax.Critere
                               where c.TypeDeCritere == "3"
                               select c;
                ddlMax.DataSource = critere3.ToList();
                ddlMax.DataTextField = "Critere1";
                ddlMax.DataBind();
                
                ddlMin.DataSource = critere3.ToList();
                ddlMin.DataTextField = "Critere1";
                ddlMin.DataBind();

                var critere4 = from Critere c in remax.Critere
                               where c.TypeDeCritere == "4"
                               select c;
                ddlNbrPiece.DataSource = critere4.ToList();
                ddlNbrPiece.DataTextField = "Critere1";
                ddlNbrPiece.DataBind();


            }

        }

     
        protected void btnTrouver_Click(object sender, EventArgs e)
        {
            ltlMaison.Text = "";
            string nbrPiece = ddlNbrPiece.SelectedItem.ToString();
            string region = ddlRegion.SelectedItem.ToString();
            int prixmax= Convert.ToInt32(ddlMax.SelectedItem.ToString());
            int prixmin = Convert.ToInt32( ddlMin.SelectedItem.ToString());
            string categorie = ddlCategorie.SelectedItem.ToString();

            var maisons = from Maison M in remax.Maison
                          where M.Type == categorie || M.Emplacement == region ||
                          M.nbrPiece.ToString() == nbrPiece || (M.Prix >= prixmin && M.Prix <= prixmax)
                          select M;

                string affichermaisons ="";
            foreach (Maison m in maisons)
            {
               
                var maisonsimage = from ImgMaison AL in remax.ImgMaison
                                   where AL.RefMaison.ToString()== m.RefMaison .ToString()
                              select AL;
                ImgMaison i = maisonsimage.First();
                affichermaisons = "  <div class='col-lg-2 col-md-5'>";
                 affichermaisons += "   <div class='single-product'>";
                  affichermaisons += "   <a href='DetailMaison.aspx?refm=" +m.RefMaison+ "' class='property-thumbnail' >";
                 affichermaisons += "<img src='" + i.Image_String + "' itemprop='photo' />";
                 affichermaisons += "          <div class='product-details'>";

                 affichermaisons += "     <h6 style='color:black;'> " + m.Type + "</h6> <br /> <h6 style='color:black;'> "+m.Emplacement+"</h6>";
                 affichermaisons += "    <div class='price'>";
                 affichermaisons += "     <h6 style='color:black;'>$" + m.Prix+"</h6>";
                 affichermaisons += "         <h6 class='l-through'>$2100000.00</h6>";
                 affichermaisons += "           </div>";
                 affichermaisons += "        <div class='prd-bottom'>";
                 affichermaisons += "           <a href = 'DetailMaison.aspx?refm=" +m.RefMaison + "' class='social-info'>";
                 affichermaisons += "          <span class='lnr lnr-move'></span>";
                 affichermaisons += "         <p class='hover-text'>view more</p>";
                 affichermaisons += "       </a>";
                 affichermaisons += " </div></div></div>   </div>";

                ltlMaison.Text += affichermaisons;


            }

        }
    }
}