﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Message.aspx.cs" Inherits="prjWebRemax.Message" %>



<!DOCTYPE html>
<html lang="fr">
	
<!-- Mirrored from www.remax-quebec.com/fr/courtiers-immobiliers/brenda.abaiov/index.rmx by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Apr 2022 01:48:38 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

		<title>BRENDA ABAIOV courtier immobilier de RE/MAX ROYAL (JORDAN) Pointe-Claire</title>

		<meta property="og:type" content="website"/>
		<meta property="og:site_name" content="RE/MAX Québec inc."/>
<meta property="og:title" content="BRENDA ABAIOV courtier immobilier de RE/MAX ROYAL (JORDAN) Pointe-Claire"/>
<meta property="og:image" content="../../../../media.remax-quebec.com/agt/contact/a9908.jpg"/>
<meta property="og:description" content="BRENDA ABAIOV courtier immobilier chez RE/MAX ROYAL (JORDAN) Pointe-Claire. Pour vendre ou acheter, condo ou maison, je suis &agrave; votre service"/>
<meta name="description" content="BRENDA ABAIOV courtier immobilier chez RE/MAX ROYAL (JORDAN) Pointe-Claire. Pour vendre ou acheter, condo ou maison, je suis &agrave; votre service"/>
<meta itemprop="image" content="../../../../media.remax-quebec.com/agt/contact/a9908.jpg"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="&#64;remaxquebec"/>
<meta name="twitter:title" content="BRENDA ABAIOV courtier immobilier de RE/MAX ROYAL (JORDAN) Pointe-Claire "/>
<meta name="twitter:description" content="BRENDA ABAIOV courtier immobilier chez RE/MAX ROYAL (JORDAN) Pointe-Claire. Pour vendre ou acheter, condo ou maison, je suis &agrave; votre service"/>
<meta name="twitter:image" content="../../../../media.remax-quebec.com/agt/contact/a9908.jpg"/>


<link rel="canonical" href="index.html" />


		<meta name="og:url" content="index.html"/>
	
		<meta name="msapplication-TileColor" content="#DC1C2E"/>
		<meta name="msapplication-TileImage" content="../../../apple-touch-icon.png"/>
		<link rel="stylesheet" href="../../../assets/remax-quebec.min-2c75f14da28a21d7db72b61339a6f98a.css"/>
		<link rel="stylesheet" href="../../../assets/css/globalHeader-a737be23ddeed7fa9574fe53de7c2647.css"/>
		<link rel="shortcut icon" href="https://www.remax-quebec.com/assets/favicon-58d8ea748f0db95504f17950b077348d.ico" type="image/x-icon" />
		<link rel="icon" href="https://www.remax-quebec.com/favicon.ico">
		<link rel="apple-touch-icon" href="../../../apple-touch-icon.png">
		<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,700%7CQuattrocento+Sans:100,300,400,700" rel="stylesheet" type="text/css">
    		<link rel="stylesheet" href="../css"/>

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<link rel="stylesheet" href="/assets/remax-quebec.ie8-e5df91385ed3ed6336d0b1366e5ea7c8.css"/>
		<![endif]-->
		<!--[if lt IE 8]>
			<link rel="stylesheet" href="/assets/remax-quebec.ie7-1c5d9ba5ca7d9c2dd4cdc2508ca8231c.css"/>
		<![endif]-->
		<script>
		var REMAXConfigs = {
			locale: "fr",
			gmap:{key:"AIzaSyBMqat-d1TFh9PRzkXZ3RPthCEa8fFi4vI"},
			page: "agent",
			
			logged: false
		};
		</script>
		<script src="../../../../ced.sascdn.com/tag/3161/smart.js" async></script>
		<script src="../../../../tagmanager.smartadserver.com/3161/243056/smart.prebid.js" async></script>
		<script>
			var sas = sas || {};
			sas.cmd = sas.cmd || [];
			sas.cmd.push(function() {
				sas.setup({ networkid: 3161, domain: "//www15.smartadserver.com", async:true, renderMode:2 });
			});
			sas.cmd.push(function() {
				sas.call("onecall", {siteId:243056, pageId:, formats: [{id:67711, tagId:"sas_67711"}, {id:67986, tagId:"sas_67986"}, {id:67715, tagId:"sas_67715"}, {id:67767, tagId:"sas_67767"}, {id:67712, tagId:"sas_67712"}], target:"pos=1;"});
			});
		</script>

		<!-- HEADER - Google Tag Manager Gendron -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'../../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-TJFLQD');</script>
		<!-- HEADER - End Google Tag Manager Gendron -->
		<!-- Google Tag Manager RE/MAX Québec-->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'../../../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KLGZ3X');</script>
		<!-- End Google Tag Manager RE/MAX Québec-->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'AW-976360684');
		</script>
		<!-- End Global site tag (gtag.js) - Google Analytics -->

	</head>
	
	
		
			<div style="padding:10px 23px 16px 18px;background-color:#dc1c2e;color:white;font-size:16px;background-image:url(../../../assets/home/messagecovid.jpg);background-size:cover;background-repeat:no-repeat;">
				<span style="color:white;font-family:Gotham Bold;font-size:22px;display:block">COVID-19</span>
				<span style="color:white;font-family:Gotham Medium;margin-top:0px;display:block;line-height:18px;">Avis et mesures mises en place par RE/MAX Québec</span>
			    <a href="https://www.remax-quebec.com/fr/infos/communique_covid19.rmx" style="font-family:Gotham Book;color:white;padding:5px 8px 3px 8px;border:1px white solid;font-size:12px;display:inline-block;margin-top:8px;">EN SAVOIR PLUS</a>
			</div>
			<!-- <div class="col-md-6" style="padding:10px 23px 16px 18px;background-color:#dc1c2e;color:white;font-size:16px;background-image:url(/assets/infos/OES_MES_2021_Photo_Maison.jpg);background-size:cover;background-repeat:no-repeat;background-position:center center">
				<img src="/assets/infos/oeslogo.png" style="float:right;">
				<span style="color:#1c3f5b;font-family:Gotham Bold;font-size:22px;display:block;">MAISON ENFANT SOLEIL</span>
				<span style="color:#1c3f5b;font-family:Gotham Medium;margin-top:0px;display:block;line-height:18px;">Découvrez la Maison Enfant Soleil 2021</span>
			    <a href="/fr/infos/oes.rmx" style="font-family:Gotham Book;color:white;padding:5px 8px 3px 8px;border:1px #da252a solid;background-color:#da252a;font-size:12px;display:inline-block;margin-top:8px;border-radius:8px">VISITEZ LA VIRTUELLEMENT ➞</a>
			</div> -->
		

	

	<body class="layout-agents" style="background-image: url(../../../assets/global/bg-pattern.png); background-repeat: repeat;">

		
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KLGZ3X" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		
		<noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-TJFLQD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


	<div class="Header">
	<div class="Header__Top">
		<div class="Header__Container">

			<div class="Header__Link monRemaxButton" data-remodal-target="my-remax">
	
				<div class="eyebrow myremax-box-closed">
					<ul class="nav nav-pills">
						<li>
							
							<a href="https://www.remax-quebec.com/fr/mon-profil.rmx" class="myremax-login">
								<i class="far fa-user-circle"></i>

								MON RE/MAX

							</a>
							<div class="myremax-box">

								<div class="myremax-box-new-account">
									<p>Nouvel utilisateur?</p>
									<form action="https://www.remax-quebec.com/fr/inscription-mon-remax.rmx">
	                                	<button type="submit" class="btn btn-logout btn-block">Cr&eacute;er un compte</button>
									</form>
								</div>
								<form class="headerloginform">
									<div class="form-group form-group-sm">
										<label for="myremax-form-email">Courriel</label>
										<input type="email" class="form-control myremax-form-email" placeholder="Votre adresse courriel" id="myremax-form-email"/>
									</div>
									<div class="form-group form-group-sm">
										<label for="myremax-form-password">Mot de passe</label>
										<input type="password" class="form-control myremax-form-password" placeholder="Mot de passe" id="myremax-form-password"/>
										<p class="help-block" style="text-align:right;"><a href="#" class="myremax-forgot-password" style="font-size:12px !important;">Mot de passe oubli&eacute;?</a></p>
                                    </div>
                                    <button type="submit" class="btn btn-block">Connexion</button>
								</form>

							</div>
						</li>
					</ul>
				</div>
			</div>
			<a href="https://global.remax.com/default.aspx?Lang=fr-FR" target="_blank">
				<div class="Header__Link globalRemax" style="border-left:0px;">
					global.remax.com
				</div>
			</a>
			<a href="https://www.remax-quebec.com/fr/infos/collection.rmx">
				<div class="Header__Link collectionHeader" style="padding: 0px 30px; border-right: 1px solid #ebebeb;">
					<img src="../../../assets/header/collection_bleu_header_fr.png" 
						alt="La Collection RE/MAX, des maisons de prestige" style="margin-top: 6px; margin-bottom: 5px; margin-right: 0px; width: auto; height: 40px;">	
				</div>
			</a>
			<a href="https://www.remax-quebec.com/fr/infos/commercial.rmx">
				<div class="Header__Link commercialHeader" style="padding: 0px 30px; border-right: 1px solid #ebebeb;">
					<img src="../../../assets/header/commercial.png" 
						alt="La Collection RE/MAX, des maisons de prestige" style="margin-top: 6px; margin-bottom: 5px; margin-right: 0px; width: auto; height: 40px;">	
				</div>
			</a>
		</div>
	</div>

	
	<div class="Header__Bottom">
		<div class="Header__Container">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand" href="index.html">
					<img src="../assets/img/header/logo.png" alt="RE/MAX Québec inc." title="RE/MAX Québec inc." class="Header__Logo" />
				</a>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Basculer la navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbar">
					<ul class="navbar-nav ml-auto">
						
						<li class="nav-item ">
							<a class="nav-link" href="WebForm1.aspx"> Chercher une maison </a>
						    </li>
						<li class="nav-item active">
							<a class="nav-link" href="Agent.aspx">Chercher un Agent </a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</div>


<div class="newBody">

		
				
 <form id="form1" runat="server">
        <div class="text-center">
            <br /><br /><br />
            Sujet :
            <asp:TextBox ID="txtSujet" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <br /><br />
            Messages<br />
            <asp:TextBox ID="Messages" runat="server" Height="278px" OnTextChanged="TextBox1_TextChanged" Width="520px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnEnvoyer"  class="btn btn-primary btn-lg" runat="server" OnClick="btnEnvoyer_Click" Text="Envoyer" />
            <br />
            <br />
            <asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Lime"></asp:Label>
        </div>
    </form>
				
						
				
			
		</div>
        <br /><br /><br />
		<div class="listing-options" id="listing">
			<div class="container">
				<div class="listing-options-inner"></div>
			</div>
		</div>
		<div class="listing" itemscope="" itemtype="https://schema.org/SearchResultsPage">
			<div class="container">
				<div class="listing-inner">
					<div class="listing-nav listing-nav-top">
						
				</div>
			</div>
		</div>
	


	<footer>
	<div class="footer-top">
		<div class="container">
			<div class="footer-top-inner">
				<div class="row">
					<div class="col-md-3 col-lg-3">
						<p class="footer-title">À vendre par région</p>
						<ul class="nav nav-stacked">
							<li>
							<a href="https://www.remax-quebec.com/fr/maison-a-vendre/rive-sud-de-montreal/resultats.rmx">
                                Maisons à vendre sur la rive-sud de Montréal
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison-a-vendre/laurentides/resultats.rmx">
                                Maisons à vendre Laurentides
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison-a-louer/laurentides/resultats.rmx"> 
                                Maisons à louer Laurentides
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/terrain-a-vendre/laurentides/resultats.rmx">
                                Terrains à vendre Laurentides
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison-a-vendre/laval-rive-nord/resultats.rmx">
                                Maisons à vendre Laval
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/proprietes-neuves/chaudieres-appalaches.rmx">
                                Maisons neuves ou condos neufs Chaudières-Appalaches
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison-a-vendre/region-de-quebec/resultats.rmx">
                                Maisons à vendre à Québec
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison-a-vendre/outaouais/resultats.rmx">
                                Maisons à vendre en Outaouais
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison-a-vendre/estrie/resultats.rmx">
                                Maisons à vendre en Estrie
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/chalet-a-vendre/estrie/resultats.rmx">
                                Chalets à vendre en Estrie
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison-a-vendre/mauricie/resultats.rmx">
                                Maisons à vendre en Mauricie
                                </a>
                            </li>
                        </ul>
                        <p class="footer-title">Courtiers immobiliers par ville</p>
                        <ul class="nav nav-pills nav-stacked">
                            <li>
                                <a href="../index5b8e.html?region=18">
                                Courtiers immobiliers sur la rive-sud de Montréal
                                </a>
                            </li>
                            <li>
                                <a href="../index125c.html?region=6">
                                Courtiers immobiliers à Montréal
                                </a>
                            </li>
                            <li>
                                <a href="../indexc8fa.html?region=3">
                                Courtiers immobiliers à Québec
                                </a>
                            </li>
                            <li>
                                <a href="../index013b.html?region=6&amp;city=66095">
                                Courtiers immobiliers à Pointe-Claire
                                </a>
                            </li>
                            <li>
                                <a href="../index9cfc.html?region=3&amp;city=23060">
                                Courtiers immobiliers à Sainte-Foy
                                </a>
                            </li>
                            <li>
                                <a href="../indexcad6.html?region=7&amp;city=81017">
                                Courtiers immobiliers à Gatineau
                                </a>
                            </li>
                            <li>
                                <a href="../indexce69.html?region=6&amp;city=66506,6650699">
                                Courtiers immobiliers à Ahuntsic-Cartierville
                                </a>
                            </li>
                            <li>
                                <a href="../indexaabb.html?region=13&amp;city=65107">
                                Courtiers immobiliers à Laval des Rapides
                                </a>
                            </li>
                            <li>
                                <a href="../index936f.html?region=18&amp;city=58227">
                                Courtiers immobiliers à Longueuil
                                </a>
                            </li>
                            <li>
                                <a href="../index6ef1.html?region=13&amp;city=65104">
                                Courtiers immobiliers à Fabreville (Laval)
                                </a>
                            </li>
                            <li>
                                <a href="../index277b.html?region=19&amp;city=25216,24010,24015">
                                Courtiers immobiliers à Lévis
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <p class="footer-title">À vendre par ville</p>
                        <ul class="nav nav-stacked">
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/gatineau/resultats.rmx">
                                Maisons à vendre à Gatineau
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/repentigny/resultats.rmx">
                                Maisons à vendre à Repentigny
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/terrebonne/resultats.rmx">
                                Maisons à vendre à Terrebonne
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/granby/resultats.rmx">
                                Maisons à vendre à Granby
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/blainville/resultats.rmx">
                                Maisons à vendre à Blainville
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/longueuil/resultats.rmx">
                                Maisons à vendre à Longueuil
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/mascouche/resultats.rmx">
                                Maisons à vendre à Mascouche
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/boucherville/resultats.rmx">
                                Maisons à vendre à Boucherville
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/victoriaville/resultats.rmx">
                                Maisons à vendre à Victoriaville
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/levis/resultats.rmx">
                                Maisons à vendre à Lévis
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/maison/drummondville/resultats.rmx">
                                Maisons à vendre à Drummondville
                                </a>
                            </li>
                        </ul>
                        <p class="footer-title">Section thématique</p>
                        <ul class="nav nav-pills nav-stacked">
                            <li>
                                <a href="https://www.remax-quebec.com/fr/reprises-de-finances/index.rmx">
                                Reprises de finances
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/proprietes-neuves/index.rmx">
                                Propriétés neuves
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/commerciale/index.rmx">
                                 Propriétés commerciales
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/visites-libres/index.rmx">
                                Visites libres
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/la-collection-remax/index.rmx">
                                La Collection RE/MAX
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/visites-virtuelles/index.rmx">
                                Visites virtuelles
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <p class="footer-title">À vendre par type de propriété</p>
                        <ul class="nav nav-pills nav-stacked">
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/residentielle/index.rmx?transacTypes=vente">
                                Maisons à vendre
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/residentielle/index.rmx?genres=1&amp;transacTypes=vente">
                                Condominiums à vendre
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/residentielle/index.rmx?genres=8&amp;transacTypes=vente">
                                Maisons mobile à vendre
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/residentielle/index.rmx?genres=7&amp;transacTypes=vente">
                                Chalets à vendre
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/ferme/index.rmx?transacTypes=vente">
                                Fermettes à vendre
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/plex/index.rmx?genres=1&amp;transacTypes=vente">
                                Duplex à vendre
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/plex/index.rmx?genres=2&amp;transacTypes=vente">
                                Triplex à vendre
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/plex/index.rmx?transacTypes=vente">
                                Multiplex à vendre
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/plex/index.rmx?genres=3&amp;transacTypes=vente">
                                Quadruplex à vendre
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/recherche/commerciale/index.rmx?transacTypes=vente">
                                Commercial à vendre
                                </a>
                            </li>
                        </ul>
                        <p class="footer-title">Section informative</p>
                        <ul class="nav nav-pills nav-stacked">
                            
                            <li>
                                <a href="https://www.remax-quebec.com/fr/conseils/achat.rmx">
                                Bien budgéter avant d&#39;acheter
                                </a>
                            </li>
                            <li>
                                <a href="http://remaxtranquilli-t.ca/oaciq.html" target="_blank" rel="noopener">
                                Protection de l&#39;OACIQ avec Tranquilli-T
                                </a>
                            </li>
                            <li>
                                <a href="http://tranquilli-t-canada.ca/" target="_blank" rel="noopener">
                                Tranquilli-T FR
                                </a>
                            </li>
                            <li>
                                <a href="http://tranquilli-t.net/" target="_blank" rel="noopener">
                                Tranquilli-T EN
                                </a>
                            </li>
                            <li>
                                <a href="https://www.remax-quebec.com/fr/infos/canafe.rmx" target="_blank" rel="noopener">
                                Lutte contre la criminalité
                                </a>
                            </li>
                        </ul>
						<p class="footer-title">Nouvelles de l&#39;immobilier</p>
						<ul class="nav nav-pills nav-stacked">

							<li><a target="_blank" href="https://moncoindevie.com/fr/article/redonner-un-air-de-jeunesse-a-sa-salle-de-bain-sans-se-ruiner-98">Redonner un air de jeunesse à sa salle de bain sans se ruiner</a></li>
							<li><a target="_blank" href="https://moncoindevie.com/fr/article/7-conseils-damenagement-pour-un-coin-devoir-stimulant-pour-vos-jeunes-97">7 conseils d’aménagement pour un coin devoir stimulant pour vos jeunes</a></li>
							<li><a target="_blank" href="https://moncoindevie.com/fr/article/innovation-les-avantages-des-visites-de-maisons-en-realite-virtuelle-87">Innovation : les avantages des visites de maisons en réalité virtuelle</a></li>
							<li><a target="_blank" href="https://moncoindevie.com/fr/article/9-activites-a-faire-a-la-maison-avec-les-enfants-88">9 activités à faire à la maison avec les enfants</a></li>

						</ul>
					</div>
                    <div class="col-md-4 col-lg-3">
                        <img src="../../../assets/global/montgolfiere-transparent-385e4648b0a2c045f0055b45777bba75.png" alt="RE/MAX Montgolfiere" class="hidden-xs hidden-sm"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-inner" itemscope itemtype="https://schema.org/LocalBusiness">
                <div class="row">
                    <div class="col-md-6">
                        <p class="copyright">
                            <span itemprop="name">RE/MAX Québec inc.</span>
                            <span itemprop="image" content="../../../../media.remax-quebec.com/email/capture_1500_cunard.png"></span>
                            <span itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                                <span itemprop="streetAddress">1500 rue Cunard</span> 
                                <span itemprop="addressLocality">Laval</span> 
                                (<span itemprop="addressRegion">Québec</span>) 
                                <span itemprop="addressCountry">Canada</span> 
                                <span itemprop="postalCode">H7S&nbsp;2B7</span> 
                                <br />T&eacute;l. : <span itemprop="telephone">450 668-7743</span> &bull;
                                Sans frais : <span itemprop="telephone">1 800 361-9325</span> &bull;
                                T&eacute;lec. : <span itemprop="faxNumber">450 668-2115</span> &bull;
                                Courriel : <a href="mailto:info@remax-quebec.com" target="_blank" rel="noopener" itemprop="email">info@remax-quebec.com</a>
                            </span>
                        </p>
                    </div>
                    <div class="col-md-6">
                        <ul class="nav nav-pills nav-justified">
                            <li itemprop="url">
                                <a href="https://www.remax-quebec.com/fr/infos/nous-joindre.rmx">
                                Nous contacter
                                </a>
                            </li>
                            <li itemprop="url">
                                <a href="https://www.remax-quebec.com/fr/infos/plan-du-site.rmx">
                                Plan du site
                                </a>
                            </li>
                            <li itemprop="url">
                                <a href="https://www.remax-quebec.com/fr/infos/politique-confidentialite.rmx">
                                Politique de confidentialit&eacute;
                                </a>
                            </li>
                            <li itemprop="url">
                                <a href="https://www.remax-quebec.com/fr/infos/politique-utilisation.rmx">
                                Politique d'utilisation
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:929183,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
</footer>

</div>

<script type="text/javascript" src="../../../assets/all.min-3fec65ad03b64bcb2ebc9bd944a25dd8.js" charset="utf-8"></script>


<!-- ASYNC comScore Tag --> 
<script> 
var _comscore = _comscore || []; 
_comscore.push({ c1: "2", c2: "3005687" }); 
(function() { 
var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true; 
s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js"; 
el.parentNode.insertBefore(s, el); 
})(); 
</script> 
<noscript><img src="https://b.scorecardresearch.com/p?c1=2%26c2=3005687%26c4=" alt="Score Card Search"/> </noscript> 
<!-- End comScore Tag -->

		<div class="modal fade" id="bs-modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
						<button type="button" class="btn btn-primary"></button>
					</div>
				</div>
			</div>
		</div>
		

    </body>

<!-- Mirrored from www.remax-quebec.com/fr/courtiers-immobiliers/brenda.abaiov/index.rmx by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Apr 2022 01:48:39 GMT -->
</html>

