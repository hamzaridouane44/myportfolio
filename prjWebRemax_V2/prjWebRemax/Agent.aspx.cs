﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

namespace prjWebRemax
{
    public partial class Agent : System.Web.UI.Page
    {
        remaxEntities remax = new remaxEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var critere2 = from Critere c in remax.Critere
                               where c.TypeDeCritere == "2"
                               select c;
                var critere1 = from Critere c in remax.Critere
                               where c.TypeDeCritere == "5"
                               select c;
                var critere0 = from Critere c in remax.Critere
                               where c.TypeDeCritere == "6"
                               select c;
                var agents = from Agents c in remax.Agents
                               
                               select c;

                ddlNumero.DataSource = agents.ToList();
                ddlNumero.DataTextField = "Numero";
                ddlNumero.DataBind();
                ddlRegion.DataSource = critere2.ToList();
                ddlRegion.DataTextField = "Critere1";
                ddlRegion.DataBind();
                ddlGenre.DataSource = critere0.ToList();
                ddlGenre.DataTextField = "Critere1";
                ddlGenre.DataBind();
                ddlLangue.DataSource = critere1.ToList();
                ddlLangue.DataTextField = "Critere1";
                ddlLangue.DataBind();

              
            }

        }

       
       protected void btnTrouve_Click(object sender, EventArgs e)
        {
             ltlMaison.Text = "";
            string Numero = ddlNumero.SelectedItem.ToString();
            string region = ddlRegion.SelectedItem.ToString();
            string langue = ddlLangue.SelectedItem.ToString();
            string Genre = ddlGenre.SelectedItem.ToString();

            var agents = from Agents M in remax.Agents
                          where M.Numero == Numero && M.Ville== region &&
                          M.Genre == Genre
                          select M;
          

            string affichermaisons = "";
            if (agents.Count() <=0)
            {
                ltlMaison.Text = "<center> <h1>Aucun Agents</h1> </center>";
            }
            foreach (Agents m in agents)
            {
                var agentsLangue = from Langue_Agent S in remax.Langue_Agent
                                   where S.refAgents.ToString() == m.RefAgent.ToString() && S.Langue == langue
                                   select S;
                if (agentsLangue.Count() <= 0)
                    {
                        ltlMaison.Text = "<center> <h1>Aucun Agents</h1> </center>";
                    }
                foreach(Langue_Agent b in agentsLangue)
                {
                var agentH = from Agents p in remax.Agents
                                             where p.RefAgent.ToString()== p.RefAgent.ToString()  && p.Numero== Numero && p.Ville == region &&
                          p.Genre == Genre
                             select p;
                   
                    foreach (Agents q in agentH)
                    {
                        affichermaisons = "     <div class='agent -entry' itemscope itemtype = 'https://schema.org/Organization' >";

                                        affichermaisons += "  <meta itemprop='name' content='RE/MAX ALLIANCE'>";

                                        affichermaisons += "<a href = 'AgentDetails.aspx?refa=" + q.RefAgent + "' class='agent-thumbnail'>";


                                        affichermaisons += "	<img class='media -object img-responsive' height=400px width=250px src='" + q.image + "' alt='GOKHAN ABAY, RE/MAX ALLIANCE'>";


                                        affichermaisons += "	</a>";

                                        affichermaisons += " <div class='agent-details' itemprop='member' itemscope itemtype = 'https://schema.org/Person' >";

                                        affichermaisons += "  <h2 ><a href=''><span itemprop = 'givenName' >"+ q.Nom + "</span></a></h2>";
                                        affichermaisons += "  <div class='agent-details-business' itemprop='address' itemscope itemtype = 'https://schema.org/PostalAddress' >";

                                        affichermaisons += "   <a href=''>RE/MAX ROYAL  inc. </a>";
                                        affichermaisons += "<span itemprop = 'addressLocality' > "+ q.Ville + " </ span >";

                                        affichermaisons += "  </ div >";

                                        affichermaisons += "  <div class='agent-details-contact'>";
                                        affichermaisons += "  <address>";
                                        affichermaisons += "<abbr title = 'Téléphone' > Bur.:</abbr> <span itemprop = 'telephone' class='phone'>" + q.NumeroTel + " </span>";
                                        affichermaisons += "  <br>";

                                        affichermaisons += "    <abbr title = 'Cellulaire' > Cell.:</abbr> <span itemprop = 'telephone' class='phone'>" + q.NumeroFixe + " </span>";
                                        affichermaisons += "   </address>";
                                        affichermaisons += " <div class='btn-group'>";
                                        affichermaisons += "  <a href = 'Message.aspx?refa=" + q.RefAgent+ "' class='btn btn-transparent-alt agent-contact' data-agent-id='11475'>";

                                        affichermaisons += "      Contactez -moi";
                                        affichermaisons += "    </a>";
                                        affichermaisons += "   <a href = 'AgentDetails.aspx?refa=" + q.RefAgent + "' class='btn btn-transparent-alt'>Voir ma page</a></div></div></div></div><br />";


                                        ltlMaison.Text += affichermaisons;
                    }
                }
                
                


            }
        }
    }
}