﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetailMaison.aspx.cs" Inherits="prjWebRemax.DetailMaison" %>


<!DOCTYPE html>
<html lang="fr">
	
<!-- Mirrored from www.remax-quebec.com/fr/index.rmx by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Mar 2022 14:59:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

		<title>Condo, chalet ou maison &agrave; vendre avec un courtier immobilier | RE/MAX Qu&eacute;bec  </title>

		<meta property="og:type" content="website"/>
		<meta property="og:site_name" content="RE/MAX Québec inc."/>
<meta property="og:title" content="Condo, chalet ou maison &agrave; vendre avec un courtier immobilier | RE/MAX Qu&eacute;bec"/>
<meta property="og:image" content="../assets/header/logo_fb.png"/>
<meta property="og:description" content="Un chalet, condo ou maison &agrave; vendre ? &Agrave; Montreal, laval, lanaudi&egrave;re ou ailleur ? Optez pour un courtier immobilier ou une agence immobili&egrave;re de RE/MAX Qu&eacute;bec."/>
<meta name="description" content="Un chalet, condo ou maison &agrave; vendre ? &Agrave; Montreal, laval, lanaudi&egrave;re ou ailleur ? Optez pour un courtier immobilier ou une agence immobili&egrave;re de RE/MAX Qu&eacute;bec."/>
<meta itemprop="image" content="../assets/header/logo_fb.png"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="&#64;remaxquebec"/>
<meta name="twitter:title" content="Condo, chalet ou maison &agrave; vendre avec un courtier immobilier | RE/MAX Qu&eacute;bec "/>
<meta name="twitter:description" content="Un chalet, condo ou maison &agrave; vendre ? &Agrave; Montreal, laval, lanaudi&egrave;re ou ailleur ? Optez pour un courtier immobilier ou une agence immobili&egrave;re de RE/MAX Qu&eacute;bec."/>
<meta name="twitter:image" content="../assets/header/logo_fb.png"/>


<link rel="canonical" href="index.html" />
    <link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/themify-icons.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/nouislider.min.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/main.css">

        <meta name="og:url" content="index.html"/>
    
		<meta name="msapplication-TileColor" content="#DC1C2E"/>
		<meta name="msapplication-TileImage" content="../apple-touch-icon.png"/>
		<link rel="stylesheet" href="../assets/remax-quebec.min-2c75f14da28a21d7db72b61339a6f98a.css"/>
		<link rel="stylesheet" href="../assets/css/globalHeader-a737be23ddeed7fa9574fe53de7c2647.css"/>
		<link rel="shortcut icon" href="https://www.remax-quebec.com/assets/favicon-58d8ea748f0db95504f17950b077348d.ico" type="image/x-icon" />
		<link rel="icon" href="https://www.remax-quebec.com/favicon.ico">
		<link rel="apple-touch-icon" href="../apple-touch-icon.png">
		<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,700%7CQuattrocento+Sans:100,300,400,700" rel="stylesheet" type="text/css">
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<link rel="stylesheet" href="/assets/remax-quebec.ie8-e5df91385ed3ed6336d0b1366e5ea7c8.css"/>
		<![endif]-->
		<!--[if lt IE 8]>
			<link rel="stylesheet" href="/assets/remax-quebec.ie7-1c5d9ba5ca7d9c2dd4cdc2508ca8231c.css"/>
		<![endif]-->
		<script>
            var REMAXConfigs = {
                locale: "fr",
                gmap: { key: "AIzaSyBMqat-d1TFh9PRzkXZ3RPthCEa8fFi4vI" },
                page: "home",

                logged: false
            };
        </script>
		<script src="../../ced.sascdn.com/tag/3161/smart.js" async></script>
		<script src="../../tagmanager.smartadserver.com/3161/243056/smart.prebid.js" async></script>
		<script>
            var sas = sas || {};
            sas.cmd = sas.cmd || [];
            sas.cmd.push(function () {
                sas.setup({ networkid: 3161, domain: "//www15.smartadserver.com", async: true, renderMode: 2 });
            });
            sas.cmd.push(function () {
                sas.call("onecall", { siteId: 243056, pageId:, formats: [{ id: 67711, tagId: "sas_67711" }, { id: 67986, tagId: "sas_67986" }, { id: 67715, tagId: "sas_67715" }, { id: 67767, tagId: "sas_67767" }, { id: 67712, tagId: "sas_67712" }], target: "pos=1;" });
            });
        </script>

		<!-- HEADER - Google Tag Manager Gendron -->
		<script>(function (w, d, s, l, i) {
                w[l] = w[l] || []; w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                }); var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                        '../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-TJFLQD');</script>
		<!-- HEADER - End Google Tag Manager Gendron -->
		<!-- Google Tag Manager RE/MAX Québec-->
		<script>(function (w, d, s, l, i) {
                w[l] = w[l] || []; w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                }); var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                        '../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-KLGZ3X');</script>
		<!-- End Google Tag Manager RE/MAX Québec-->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
		<script>
            window.dataLayer = window.dataLayer || [];
            function gtag() { dataLayer.push(arguments); }
            gtag('js', new Date());
            gtag('config', 'AW-976360684');
        </script>
		<!-- End Global site tag (gtag.js) - Google Analytics -->

	</head>
	
	 <form id="form1" runat="server">
		
			<div style="padding:10px 23px 16px 18px;background-color:#dc1c2e;color:white;font-size:16px;background-image:url(../assets/home/messagecovid.jpg);background-size:cover;background-repeat:no-repeat;">
				<span style="color:white;font-family:Gotham Bold;font-size:22px;display:block">COVID-19</span>
				<span style="color:white;font-family:Gotham Medium;margin-top:0px;display:block;line-height:18px;">Avis et mesures mises en place par RE/MAX Québec</span>
			</div>
			<body class="layout-home" style="background-image: url(../assets/global/bg-pattern.png); background-repeat: repeat;">

		
		<noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-TJFLQD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


	<div class="Header">
	<div class="Header__Top">
		<div class="Header__Container">

			<div class="Header__Link monRemaxButton" data-remodal-target="my-remax">
	
				<div class="eyebrow myremax-box-closed">
					<p>
                        &nbsp;</p>
				</div>
			</div>
			<a href="#" target="_blank">
				<div class="Header__Link globalRemax" style="border-left:0px;">
					global.remax.com
				</div>
			</a>
			<a href="#">
				<div class="Header__Link collectionHeader" style="padding: 0px 30px; border-right: 1px solid #ebebeb;">
					<img src="../assets/header/collection_bleu_header_fr.png" 
						alt="La Collection RE/MAX, des maisons de prestige" style="margin-top: 6px; margin-bottom: 5px; margin-right: 0px; width: auto; height: 40px;">	
				</div>
			</a>
			<a href="#">
				<div class="Header__Link commercialHeader" style="padding: 0px 30px; border-right: 1px solid #ebebeb;">
					<img src="../assets/header/commercial.png" 
						alt="La Collection RE/MAX, des maisons de prestige" style="margin-top: 6px; margin-bottom: 5px; margin-right: 0px; width: auto; height: 40px;">	
				</div>
			</a>
		</div>
	</div>

	<div class="Header__Bottom">
		<div class="Header__Container">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand" href="index.html">
					<img src="../assets/img/header/logo.png" alt="RE/MAX Québec inc." title="RE/MAX Québec inc." class="Header__Logo" />
				</a>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Basculer la navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbar">
					<ul class="navbar-nav ml-auto">
						
						<li class="nav-item ">
							<a class="nav-link" href="WebForm1.aspx"> Chercher une maison </a>
						    </li>
						<li class="nav-item ">
							<a class="nav-link" href="#">Chercher un Agent
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</div>



        <div class="hero">
            <div class="container"> 
                <div class="hero-inner">
					 	<div data-ride="carousel" class="carousel slide" id="carousel-main">
					      <ol class="carousel-indicators">
					        <li class="active" data-slide-to="0" data-target="#carousel-main"></li>
					        <li data-slide-to="1" data-target="#carousel-main" class=""></li>
					        <li data-slide-to="2" data-target="#carousel-main" class=""></li>
					        <li data-slide-to="3" data-target="#carousel-main" class=""></li>
					      </ol>
					      
					
					
									      <div class="carousel-inner" style="font-size:40px; font-weight:bold;">
											  <h2> <asp:Literal ID="lblInfo" runat="server"></asp:Literal> </h2>
											    <asp:Literal ID="ltlMaison2" runat="server"></asp:Literal> 
										        <asp:Literal ID="ltlMaison" runat="server"></asp:Literal> 
											 <h3> <asp:Literal ID="lblinfo2" runat="server"></asp:Literal> </h3> 
										    
									        </div>
									        
                    			        
					      <a data-slide="prev" href="#carousel-main" class="left carousel-control">
					        <span class="glyphicon glyphicon-chevron-left"></span>
					      </a>
					      <a data-slide="next" href="#carousel-main" class="right carousel-control">
					        <span class="glyphicon glyphicon-chevron-right"></span>
					      </a>
					      
					</div>
                </div>
            </div>
        </div>
				
					<center><h2>Agent(s) Responsable(s) de la maison</h2>
				 <asp:Literal ID="ltlAgent" runat="server"></asp:Literal> 
						</center>
        <div class="menu">
            <a href="#" class="primary"><h3>Nos propri&eacute;t&eacute;s.</h3></a>
            <a href="#"><h3>Nos courtiers.</h3></a>
            <a href="#"><h3>Nos programmes.</h3></a>
            <a href="#"><h3>Nos r&eacute;gions.</h3></a>
        </div>
        <div class="listing">
            <div class="container">
                <div class="listing-inner">
                  
                    <div class="listing-mosaik listing-properties">
                   		<input type="hidden" id="isGeoloc" value="0">
                        

                        
                       
        
    
                </div>
            </div>
        </div>
        <div class="advantages">
            <div class="container">
                <div class="advantages-inner">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="advantages-thumbnail">
                                <a href="#"><img src="../assets/home/advantages-home-8e97c2cc681ae9d9bf967036dfa49161.jpg" alt="Maisons Neuves"/></a>
                            </div>
                            <h3><a href="proprietes-neuves/index.html">Maisons Neuves.</a></h3>
                            <p>Consultez la liste, par projets, des propri&eacute;t&eacute;s neuves &agrave; vendre.</p>
                        </div>
                        <div class="col-md-3">
                            <div class="advantages-thumbnail">
                                <a href="#"><img src="../assets/home/advantages-commerce-a71f91c23e93b173da02a05d11b2ed1e.jpg" alt="Commercial." /></a>
                            </div>
                            <h3>
                            <a href="#">Commerciale.</a></h3>
                            <p>Trouvez le multilogement, le commerce, la b&acirc;tisse industrielle, le terrain ou la ferme qui r&eacute;pondra &agrave; vos besoins.</p>
                        </div>
                        <div class="col-md-3">
                            <div class="advantages-thumbnail">
                                <a href="#"><img src="../assets/home/advantages-open-houses-9328b773aebbd8ae6d4f00e0b2fdc411.jpg" alt="Visites Libres." /></a>
                            </div>
                            <h3>
                            <a href="#">Visites Libres.</a></h3>
                            <p>Consultez la liste des prochaines visites libres de votre r&eacute;gion.</p>
                        </div>
                        <div class="col-md-3">
                            <div class="advantages-thumbnail">
                                <a href="#"><img src="../assets/home/advantages-collection-fr-ee9467b52dc693caa5e9c39c77abc849.jpg" alt="La Collection." /></a>
                            </div>
                            <h3>
                            <a href="la-collection-remax/index.html">
                            La Collection.
                            </a></h3>
                            <p>D&eacute;couvrez notre collection de propri&eacute;t&eacute;s de prestige, sign&eacute;e RE/MAX</p>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="regions">
            <div class="container">
                <div class="regions-inner">
                    <h3>
                    <a href="regions/index.html">
                    D&eacute;couvrir les r&eacute;gions.
                    </a></h3>
                    <a href="regions/index.html" class="btn btn-transparent btn-lg">
                    Recherchez
                    </a>
                </div>
            </div>
        </div>
        <div class="blogs">
 <div class="container">

 </div>
</div>
        
    




</div>

<script type="text/javascript" src="../assets/all.min-3fec65ad03b64bcb2ebc9bd944a25dd8.js" charset="utf-8"></script>


<!-- ASYNC comScore Tag --> 
<script> 
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "3005687" });
    (function () {
        var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
        s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
        el.parentNode.insertBefore(s, el);
    })(); 
</script> 
<noscript><img src="https://b.scorecardresearch.com/p?c1=2%26c2=3005687%26c4=" alt="Score Card Search"/> </noscript> 
<!-- End comScore Tag -->

		<div class="modal fade" id="bs-modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
						<button type="button" class="btn btn-primary"></button>
					</div>
				</div>
			</div>
		</div>
		 </form>

    </body>

<!-- Mirrored from www.remax-quebec.com/fr/index.rmx by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Mar 2022 15:00:06 GMT -->
</html>
