﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="prjWebRemax.WebForm1" %>


<!DOCTYPE html>
<html lang="fr">
	
<!-- Mirrored from www.remax-quebec.com/fr/index.rmx by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Mar 2022 14:59:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

		<title>Condo, chalet ou maison &agrave; vendre avec un courtier immobilier | RE/MAX Qu&eacute;bec  </title>

		<meta property="og:type" content="website"/>
		<meta property="og:site_name" content="RE/MAX Québec inc."/>
<meta property="og:title" content="Condo, chalet ou maison &agrave; vendre avec un courtier immobilier | RE/MAX Qu&eacute;bec"/>
<meta property="og:image" content="../assets/header/logo_fb.png"/>
<meta property="og:description" content="Un chalet, condo ou maison &agrave; vendre ? &Agrave; Montreal, laval, lanaudi&egrave;re ou ailleur ? Optez pour un courtier immobilier ou une agence immobili&egrave;re de RE/MAX Qu&eacute;bec."/>
<meta name="description" content="Un chalet, condo ou maison &agrave; vendre ? &Agrave; Montreal, laval, lanaudi&egrave;re ou ailleur ? Optez pour un courtier immobilier ou une agence immobili&egrave;re de RE/MAX Qu&eacute;bec."/>
<meta itemprop="image" content="../assets/header/logo_fb.png"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="&#64;remaxquebec"/>
<meta name="twitter:title" content="Condo, chalet ou maison &agrave; vendre avec un courtier immobilier | RE/MAX Qu&eacute;bec "/>
<meta name="twitter:description" content="Un chalet, condo ou maison &agrave; vendre ? &Agrave; Montreal, laval, lanaudi&egrave;re ou ailleur ? Optez pour un courtier immobilier ou une agence immobili&egrave;re de RE/MAX Qu&eacute;bec."/>
<meta name="twitter:image" content="../assets/header/logo_fb.png"/>


<link rel="canonical" href="index.html" />
    <link rel="stylesheet" href="css/linearicons.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/themify-icons.css">
	<link rel="stylesheet" href="css/nice-select.css">
	<link rel="stylesheet" href="css/nouislider.min.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/main.css">

        <meta name="og:url" content="index.html"/>
    
		<meta name="msapplication-TileColor" content="#DC1C2E"/>
		<meta name="msapplication-TileImage" content="../apple-touch-icon.png"/>
		<link rel="stylesheet" href="../assets/remax-quebec.min-2c75f14da28a21d7db72b61339a6f98a.css"/>
		<link rel="stylesheet" href="../assets/css/globalHeader-a737be23ddeed7fa9574fe53de7c2647.css"/>
		<link rel="shortcut icon" href="https://www.remax-quebec.com/assets/favicon-58d8ea748f0db95504f17950b077348d.ico" type="image/x-icon" />
		<link rel="icon" href="https://www.remax-quebec.com/favicon.ico">
		<link rel="apple-touch-icon" href="../apple-touch-icon.png">
		<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,400,700%7CQuattrocento+Sans:100,300,400,700" rel="stylesheet" type="text/css">
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<link rel="stylesheet" href="/assets/remax-quebec.ie8-e5df91385ed3ed6336d0b1366e5ea7c8.css"/>
		<![endif]-->
		<!--[if lt IE 8]>
			<link rel="stylesheet" href="/assets/remax-quebec.ie7-1c5d9ba5ca7d9c2dd4cdc2508ca8231c.css"/>
		<![endif]-->
		<script>
            var REMAXConfigs = {
                locale: "fr",
                gmap: { key: "AIzaSyBMqat-d1TFh9PRzkXZ3RPthCEa8fFi4vI" },
                page: "home",

                logged: false
            };
        </script>
		<script src="../../ced.sascdn.com/tag/3161/smart.js" async></script>
		<script src="../../tagmanager.smartadserver.com/3161/243056/smart.prebid.js" async></script>
		<script>
            var sas = sas || {};
            sas.cmd = sas.cmd || [];
            sas.cmd.push(function () {
                sas.setup({ networkid: 3161, domain: "//www15.smartadserver.com", async: true, renderMode: 2 });
            });
            sas.cmd.push(function () {
                sas.call("onecall", { siteId: 243056, pageId:, formats: [{ id: 67711, tagId: "sas_67711" }, { id: 67986, tagId: "sas_67986" }, { id: 67715, tagId: "sas_67715" }, { id: 67767, tagId: "sas_67767" }, { id: 67712, tagId: "sas_67712" }], target: "pos=1;" });
            });
        </script>

		<!-- HEADER - Google Tag Manager Gendron -->
		<script>(function (w, d, s, l, i) {
                w[l] = w[l] || []; w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                }); var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                        '../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-TJFLQD');</script>
		<!-- HEADER - End Google Tag Manager Gendron -->
		<!-- Google Tag Manager RE/MAX Québec-->
		<script>(function (w, d, s, l, i) {
                w[l] = w[l] || []; w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                }); var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                        '../../www.googletagmanager.com/gtm5445.html?id=' + i + dl; f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-KLGZ3X');</script>
		<!-- End Google Tag Manager RE/MAX Québec-->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script>
		<script>
            window.dataLayer = window.dataLayer || [];
            function gtag() { dataLayer.push(arguments); }
            gtag('js', new Date());
            gtag('config', 'AW-976360684');
        </script>
		<!-- End Global site tag (gtag.js) - Google Analytics -->

	</head>
	
	 <form id="form1" runat="server">
		
			<div style="padding:10px 23px 16px 18px;background-color:#dc1c2e;color:white;font-size:16px;background-image:url(../assets/home/messagecovid.jpg);background-size:cover;background-repeat:no-repeat;">
				<span style="color:white;font-family:Gotham Bold;font-size:22px;display:block">COVID-19</span>
				<span style="color:white;font-family:Gotham Medium;margin-top:0px;display:block;line-height:18px;">Avis et mesures mises en place par RE/MAX Québec</span>
			</div>
			<body class="layout-home" style="background-image: url(../assets/global/bg-pattern.png); background-repeat: repeat;">

		
		<noscript><iframe src="http://www.googletagmanager.com/ns.html?id=GTM-TJFLQD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


	<div class="Header">
	<div class="Header__Top">
		<div class="Header__Container">

			<div class="Header__Link monRemaxButton" data-remodal-target="my-remax">
	
				<div class="eyebrow myremax-box-closed">
					<p>
                        &nbsp;</p>
				</div>
			</div>
			<a href="#" target="_blank">
				<div class="Header__Link globalRemax" style="border-left:0px;">
					global.remax.com
				</div>
			</a>
			<a href="#">
				<div class="Header__Link collectionHeader" style="padding: 0px 30px; border-right: 1px solid #ebebeb;">
					<img src="../assets/header/collection_bleu_header_fr.png" 
						alt="La Collection RE/MAX, des maisons de prestige" style="margin-top: 6px; margin-bottom: 5px; margin-right: 0px; width: auto; height: 40px;">	
				</div>
			</a>
			<a href="#">
				<div class="Header__Link commercialHeader" style="padding: 0px 30px; border-right: 1px solid #ebebeb;">
					<img src="../assets/header/commercial.png" 
						alt="La Collection RE/MAX, des maisons de prestige" style="margin-top: 6px; margin-bottom: 5px; margin-right: 0px; width: auto; height: 40px;">	
				</div>
			</a>
		</div>
	</div>

	<div class="Header__Bottom">
		<div class="Header__Container">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand" href="index.html">
					<img src="../assets/img/header/logo.png" alt="RE/MAX Québec inc." title="RE/MAX Québec inc." class="Header__Logo" />
				</a>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Basculer la navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbar">
					<ul class="navbar-nav ml-auto">
						
						<li class="nav-item active">
							<a class="nav-link" href="WebForm1.aspx"> Chercher une maison </a>
						    </li>
						<li class="nav-item ">
							<a class="nav-link" href="Agent.aspx">Chercher un Agent </a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</div>


<div class="newBody">

        <div class="masthead spring day">
            <div class="masthead-container">
                <div class="masthead-content">
                    <div class="container">
                        <div class="masthead-inner">
                            <p class="masthead-title">Trouvez votre maison de r&ecirc;ve.</p>
                            <div class="search">
                                ﻿<form method="post" action="#" id="recherche_form">
    <input type="hidden" id="lang" value="fr"/>
    <input type="hidden" name="mode" value="criterias" id="search-mode"/>
   <div class="input-search">
       <h1>&Eacute;crivez ce que vous cherchez. Maison &agrave; vendre</h1>
       <div class="input-container">
            <input type="text" name="query" placeholder="" />
       </div>
       <div class="shape-triangle"></div>
   </div>
   <div class="options">
       <a href="#" class="btn btn-more-criteria">
       + de crit&egrave;res
       </a>
   </div>
   
   <div class="more-criteria" style="display:none;">
      <div class="criterias-section-1">
         
             
          <div style="align:center;"><asp:DropDownList ID="ddlCategorie" runat="server" class="ms-select single" data-placeholder="Catégorie" name="categorie" >
                    <asp:ListItem>hamza</asp:ListItem>
                </asp:DropDownList>
               <asp:DropDownList ID="ddlRegion" runat="server"  class="ms-select single" data-placeholder="Emplacement" name="Emplacement" >
                    <asp:ListItem>Canada</asp:ListItem>
                </asp:DropDownList>
             <asp:DropDownList ID="ddlMin" runat="server"  class="ms-select single" data-placeholder="Prix Minimum" name="prix_min" >
                    <asp:ListItem>Canada</asp:ListItem>
                </asp:DropDownList>
              <asp:DropDownList ID="ddlMax" runat="server"  class="ms-select single" data-placeholder="Prix maximum" name="prix_max" >
                    <asp:ListItem>Canada</asp:ListItem>
                </asp:DropDownList>
              <asp:DropDownList ID="ddlNbrPiece" runat="server" class="ms-select single" data-placeholder="nombre de piece " name="nbr_piece" >
                    <asp:ListItem>Canada</asp:ListItem>
                </asp:DropDownList>

              <asp:Button ID="btnTrouver" runat="server" OnClick="btnTrouver_Click" Text="Trouver"  class="btn btn-primary btn-lg" />
              <br /><br /><br />
              <asp:GridView ID="gridMaison" runat="server" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" CssClass="auto-style18" ForeColor="Black" GridLines="Vertical" Width="711px">
                  <AlternatingRowStyle BackColor="White" />
                  <FooterStyle BackColor="#CCCC99" />
                  <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                  <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                  <RowStyle BackColor="#F7F7DE" />
                  <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                  <SortedAscendingCellStyle BackColor="#FBFBF2" />
                  <SortedAscendingHeaderStyle BackColor="#848384" />
                  <SortedDescendingCellStyle BackColor="#EAEAD3" />
                  <SortedDescendingHeaderStyle BackColor="#575357" />
              </asp:GridView>
                                        
          </div>
              
</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <asp:Literal ID="ltlMaison" runat="server"></asp:Literal> 
        <div class="hero">
            <div class="container"> 
                <div class="hero-inner">
					 	<div data-ride="carousel" class="carousel slide" id="carousel-main">
					      <ol class="carousel-indicators">
					        <li class="active" data-slide-to="0" data-target="#carousel-main"></li>
					        <li data-slide-to="1" data-target="#carousel-main" class=""></li>
					        <li data-slide-to="2" data-target="#carousel-main" class=""></li>
					        <li data-slide-to="3" data-target="#carousel-main" class=""></li>
					      </ol>

									      <div class="carousel-inner" style="font-size:40px; font-weight:bold;">
										        <div class="item active" align="center">
										        	<a href="#" target="_blank" id="Blogue">
										        		<img src="../assets/home/carousel_blog_fr-2dc98fafa54efb2ce75b0134a1fb95d8.jpg" alt="Blogue">
										        	</a>
										        </div>
										        <div class="item " align="center">
										        	<a href="#" id="Collection">
										        		<img src="../assets/home/carousel_collection_fr-c776c1b01704bdb02d5322df48479257.jpg" alt="la collection">
										        	</a>
										        </div>
										        <div class="item" align="center">
										        	<a href="#" id="Tranquilli-T">
										        		<img src="../assets/home/carousel_tranquillit_fr-0fd2b270fb612300c3175f2081489dee.jpg" alt="Tranquilli-t">
										        	</a>
										        </div>
										        <div class="item" align="center">
										        	<a href="#" id="Integri-t">
										        		<img src="../assets/home/carousel_integrit_fr-01e35a6010b77fde626e37cb6673ca5b.jpg" alt="Integri-t">
										        	</a>
										        </div>
									        </div>
									        
                    			        
					      <a data-slide="prev" href="#carousel-main" class="left carousel-control">
					        <span class="glyphicon glyphicon-chevron-left"></span>
					      </a>
					      <a data-slide="next" href="#carousel-main" class="right carousel-control">
					        <span class="glyphicon glyphicon-chevron-right"></span>
					      </a>
					      
					</div>
                </div>
            </div>
        </div>
        <div class="menu">
            <a href="#" class="primary"><h3>Nos propri&eacute;t&eacute;s.</h3></a>
            <a href="#"><h3>Nos courtiers.</h3></a>
            <a href="#"><h3>Nos programmes.</h3></a>
            <a href="#"><h3>Nos r&eacute;gions.</h3></a>
        </div>
        <div class="listing">
            <div class="container">
                <div class="listing-inner">
                  
                    <div class="listing-mosaik listing-properties">
                   		<input type="hidden" id="isGeoloc" value="0">
                </div>
            </div>
        </div>
        <div class="advantages">
            <div class="container">
                <div class="advantages-inner">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="advantages-thumbnail">
                                <a href="#"><img src="../assets/home/advantages-home-8e97c2cc681ae9d9bf967036dfa49161.jpg" alt="Maisons Neuves"/></a>
                            </div>
                            <h3><a href="proprietes-neuves/index.html">Maisons Neuves.</a></h3>
                            <p>Consultez la liste, par projets, des propri&eacute;t&eacute;s neuves &agrave; vendre.</p>
                        </div>
                        <div class="col-md-3">
                            <div class="advantages-thumbnail">
                                <a href="#"><img src="../assets/home/advantages-commerce-a71f91c23e93b173da02a05d11b2ed1e.jpg" alt="Commercial." /></a>
                            </div>
                            <h3>
                            <a href="#">Commerciale.</a></h3>
                            <p>Trouvez le multilogement, le commerce, la b&acirc;tisse industrielle, le terrain ou la ferme qui r&eacute;pondra &agrave; vos besoins.</p>
                        </div>
                        <div class="col-md-3">
                            <div class="advantages-thumbnail">
                                <a href="#"><img src="../assets/home/advantages-open-houses-9328b773aebbd8ae6d4f00e0b2fdc411.jpg" alt="Visites Libres." /></a>
                            </div>
                            <h3>
                            <a href="#">Visites Libres.</a></h3>
                            <p>Consultez la liste des prochaines visites libres de votre r&eacute;gion.</p>
                        </div>
                        <div class="col-md-3">
                            <div class="advantages-thumbnail">
                                <a href="#"><img src="../assets/home/advantages-collection-fr-ee9467b52dc693caa5e9c39c77abc849.jpg" alt="La Collection." /></a>
                            </div>
                            <h3>
                            <a href="la-collection-remax/index.html">
                            La Collection.
                            </a></h3>
                            <p>D&eacute;couvrez notre collection de propri&eacute;t&eacute;s de prestige, sign&eacute;e RE/MAX</p>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="regions">
            <div class="container">
                <div class="regions-inner">
                    <h3>
                    <a href="regions/index.html">
                    D&eacute;couvrir les r&eacute;gions.
                    </a></h3>
                    <a href="regions/index.html" class="btn btn-transparent btn-lg">
                    Recherchez
                    </a>
                </div>
            </div>
        </div>
        <div class="blogs">
 <div class="container">

 </div>
</div>
        
    


	<footer>
	<div class="footer-top">
		<div class="container">
			<div class="footer-top-inner">
				<div class="row">
					<div class="col-md-3 col-lg-3">
						<p class="footer-title">À vendre par région</p>
						<ul class="nav nav-stacked">
							<li>
							<a href="#">
                                Maisons à vendre sur la rive-sud de Montréal
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre Laurentides
                                </a>
                            </li>
                            <li>
                                <a href="#"> 
                                Maisons à louer Laurentides
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Terrains à vendre Laurentides
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre Laval
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons neuves ou condos neufs Chaudières-Appalaches
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Québec
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre en Outaouais
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre en Estrie
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Chalets à vendre en Estrie
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre en Mauricie
                                </a>
                            </li>
                        </ul>
                        <p class="footer-title">Courtiers immobiliers par ville</p>
                        <ul class="nav nav-pills nav-stacked">
                            <li>
                                <a href="#">
                                Courtiers immobiliers sur la rive-sud de Montréal
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Montréal
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Québec
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Pointe-Claire
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Sainte-Foy
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Gatineau
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Ahuntsic-Cartierville
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Laval des Rapides
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Longueuil
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Fabreville (Laval)
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Courtiers immobiliers à Lévis
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <p class="footer-title">À vendre par ville</p>
                        <ul class="nav nav-stacked">
                            <li>
                                <a href="#">
                                Maisons à vendre à Gatineau
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Repentigny
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Terrebonne
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Granby
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Blainville
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Longueuil
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Mascouche
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Boucherville
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Victoriaville
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Lévis
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons à vendre à Drummondville
                                </a>
                            </li>
                        </ul>
                        <p class="footer-title">Section thématique</p>
                        <ul class="nav nav-pills nav-stacked">
                            <li>
                                <a href="#">
                                Reprises de finances
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Propriétés neuves
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                 Propriétés commerciales
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Visites libres
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                La Collection RE/MAX
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Visites virtuelles
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <p class="footer-title">À vendre par type de propriété</p>
                        <ul class="nav nav-pills nav-stacked">
                            <li>
                                <a href="#">
                                Maisons à vendre
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Condominiums à vendre
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Maisons mobile à vendre
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Chalets à vendre
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Fermettes à vendre
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Duplex à vendre
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Triplex à vendre
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Multiplex à vendre
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Quadruplex à vendre
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                Commercial à vendre
                                </a>
                            </li>
                        </ul>
                        <p class="footer-title">Section informative</p>
                        <ul class="nav nav-pills nav-stacked">
                            
                            <li>
                                <a href="#">
                                Bien budgéter avant d&#39;acheter
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank" rel="noopener">
                                Protection de l&#39;OACIQ avec Tranquilli-T
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank" rel="noopener">
                                Tranquilli-T FR
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank" rel="noopener">
                                Tranquilli-T EN
                                </a>
                            </li>
                            <li>
                                <a href="infos/canafe.html" target="_blank" rel="noopener">
                                Lutte contre la criminalité
                                </a>
                            </li>
                        </ul>
						<p class="footer-title">Nouvelles de l&#39;immobilier</p>
						<ul class="nav nav-pills nav-stacked">

							<li><a target="_blank" href="#">Redonner un air de jeunesse à sa salle de bain sans se ruiner</a></li>
							<li><a target="_blank" href="#">7 conseils d’aménagement pour un coin devoir stimulant pour vos jeunes</a></li>
							<li><a target="_blank" href="#">Innovation : les avantages des visites de maisons en réalité virtuelle</a></li>
							<li><a target="_blank" href="#">9 activités à faire à la maison avec les enfants</a></li>

						</ul>
					</div>
                    <div class="col-md-4 col-lg-3">
                        <img src="../assets/global/montgolfiere-transparent-385e4648b0a2c045f0055b45777bba75.png" alt="RE/MAX Montgolfiere" class="hidden-xs hidden-sm"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-inner" itemscope itemtype="https://schema.org/LocalBusiness">
                <div class="row">
                    <div class="col-md-6">
                        <p class="copyright">
                            <span itemprop="name">RE/MAX Québec inc.</span>
                            <span itemprop="image" content="../../media.remax-quebec.com/email/capture_1500_cunard.png"></span>
                            <span itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">
                                <span itemprop="streetAddress">1500 rue Cunard</span> 
                                <span itemprop="addressLocality">Laval</span> 
                                (<span itemprop="addressRegion">Québec</span>) 
                                <span itemprop="addressCountry">Canada</span> 
                                <span itemprop="postalCode">H7S&nbsp;2B7</span> 
                                <br />T&eacute;l. : <span itemprop="telephone">450 668-7743</span> &bull;
                                Sans frais : <span itemprop="telephone">1 800 361-9325</span> &bull;
                                T&eacute;lec. : <span itemprop="faxNumber">450 668-2115</span> &bull;
                                Courriel : <a href="mailto:info@remax-quebec.com" target="_blank" rel="noopener" itemprop="email">info@remax-quebec.com</a>
                            </span>
                        </p>
                    </div>
                    <div class="col-md-6">
                        <ul class="nav nav-pills nav-justified">
                            <li itemprop="url">
                                <a href="infos/nous-joindre.html">
                                Nous contacter
                                </a>
                            </li>
                            <li itemprop="url">
                                <a href="infos/plan-du-site.html">
                                Plan du site
                                </a>
                            </li>
                            <li itemprop="url">
                                <a href="infos/politique-confidentialite.html">
                                Politique de confidentialit&eacute;
                                </a>
                            </li>
                            <li itemprop="url">
                                <a href="infos/politique-utilisation.html">
                                Politique d'utilisation
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () { (h.hj.q = h.hj.q || []).push(arguments) };
            h._hjSettings = { hjid: 929183, hjsv: 6 };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
</footer>

</div>

<script type="text/javascript" src="../assets/all.min-3fec65ad03b64bcb2ebc9bd944a25dd8.js" charset="utf-8"></script>


<!-- ASYNC comScore Tag --> 
<script> 
    var _comscore = _comscore || [];
    _comscore.push({ c1: "2", c2: "3005687" });
    (function () {
        var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
        s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
        el.parentNode.insertBefore(s, el);
    })(); 
</script> 
<noscript><img src="https://b.scorecardresearch.com/p?c1=2%26c2=3005687%26c4=" alt="Score Card Search"/> </noscript> 
<!-- End comScore Tag -->

		<div class="modal fade" id="bs-modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
						<button type="button" class="btn btn-primary"></button>
					</div>
				</div>
			</div>
		</div>
		 </form>

    </body>

<!-- Mirrored from www.remax-quebec.com/fr/index.rmx by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Mar 2022 15:00:06 GMT -->
</html>


